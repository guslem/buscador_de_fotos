import React, { useState, useEffect } from "react";
import Form from "./Components/Form";
import ListImages from "./Components/ListImages";

function App() {
  //datos de busqueda
  const [srchImg, updatesrchImg] = useState({});
  //datos obtenidos en la busqueda
  const [SaveSrch, updateSaveSrch] = useState([]);
  //indica si realiza el llamado a la api
  const [Show, updateShow] = useState(false);

  //manejo de paginados
  const [ActualPage, SaveActualPage] = useState(1);
  const [TotalPage, SaveTotalPage] = useState(1);

  const { SrchTxt, SrchCat } = srchImg;

  const BackPage = () => {
    const backPage = ActualPage - 1;

    if (backPage === 0) return;

    SaveActualPage(backPage);
  };
  const NextPage = () => {
    const nextPage = ActualPage + 1;

    if (nextPage > TotalPage) return;

    SaveActualPage(nextPage);
  };

  //al cambiar el valor atado al useEffect se dispara la consulta
  useEffect(() => {
    const callApi = async () => {
      if (Show === false) return;
      const imagexPage = 20;
      let url = "";
      const key = "17825780-efda477c066c8aa6e6c1741de";
      if (SrchCat === "All") {
        url = `https://pixabay.com/api/?key=${key}&q=${SrchTxt}&per_page=${imagexPage}&page=${ActualPage}`;
      } else {
        url = `https://pixabay.com/api/?key=${key}&q=${SrchTxt}&category=${SrchCat}&per_page=${imagexPage}&page=${ActualPage}`;
      }
      const Response = await fetch(url);

      const Result = await Response.json();

      updateSaveSrch(Result.hits);
      //obtengo el numero de paginas
      const TotalPageReturn = Math.ceil(Result.totalHits / imagexPage);
      SaveTotalPage(TotalPageReturn);
    };
    //uso jquery para posicionarme en el elemento principal de la pagina
    const ScrollPageInit = document.querySelector(".jumbotron");
    ScrollPageInit.scrollIntoView({ behavior: "smooth" });

    callApi();
  }, [srchImg, ActualPage]);

  return (
    <div className="jumbotron">
      <p className="lead text-center">Buscador de imagen</p>
      <Form updatesrchImg={updatesrchImg} updateShow={updateShow} />

      <div className="row justify-content-center">
        <ListImages SaveSrch={SaveSrch} />
        {ActualPage === 1 ? null : (
          <button
            type="button"
            onClick={BackPage}
            className="bbtn btn-info mr-1"
          >
            &laquo;Anterior
          </button>
        )}
        {ActualPage === TotalPage ? null : (
          <button
            type="button"
            onClick={NextPage}
            className="bbtn btn-info mr-1"
          >
            Siguiente&raquo;
          </button>
        )}
      </div>
    </div>
  );
}

export default App;
