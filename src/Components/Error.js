import React from "react";

const Error = ({ message }) => {
  return (
    <div className="alert-dismissible alert alert-warning">
      <h4 className="alert-heading">{message}</h4>
      <p className="mb-1">
        Para realizar la busqueda debe ingresar una palabra, por ejemplo: Rosa
        roja
      </p>
    </div>
  );
};

export default Error;
