import React from "react";
import Images from "./Images";

const ListImages = ({ SaveSrch }) => {
  return (
    <div className="col-12 p-5 row">
      {SaveSrch.map((image) => (
        <Images key={image.key} image={image} />
      ))}
    </div>
  );
};

export default ListImages;
