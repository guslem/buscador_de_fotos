import React, { useState } from "react";
import Error from "./Error";
import styled from "@emotion/styled";

const Container = styled.div`
  max-width: 2000px;
  /*padding-left:10%;*/
  @media (min-width: 200px) {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    columns-gap: 1rem;
  }
`;

const Form = ({ updatesrchImg, updateShow }) => {
  // state inicial
  const [Data, updateData] = useState({
    SrchTxt: "",
    SrchCat: "",
  });

  //extraigo las variables
  const { SrchTxt, SrchCat } = Data;
  //extraigo las variables
  // const {updSrchTxt, updSrchCat} = updatesrchImg;

  const [ErrorMsj, updateErrorMsj] = useState(false);

  const [ValidInput, updateValidInput] = useState(
    "form-control form-control-lg"
  );

  //al escribit o seleccionar del combo guardo el resultado
  const onChangeInput = (e) => {
    console.log("click 1");

    updateData({
      ...Data,
      [e.target.name]: e.target.value,
    });
  };
  const SrchImg = (e) => {
    e.preventDefault();
    console.log("valido ingresos");
    if (SrchTxt === "" || SrchCat === "") {
      updateErrorMsj(true);
      updateValidInput("form-control form-control-lg is-invalid");
    } else {
      updateValidInput("form-control form-control-lg is-valid");
      updateErrorMsj(false);
      //updatesrchImg=Data;

      //        console.log(Data)
      updatesrchImg(Data);
      updateShow(true);
      console.log("dentor del form 1");
    }

    /*

        if (SrchTxt === '') { 
            updateValidInput("form-control form-control-lg is-invalid")      
            updateErrorMsj(true);      
            return;
        }else {
            updateValidInput("form-control form-control-lg is-valid")
            updateErrorMsj(false);
            updatesrchImg=Data;
           
          
        }*/
  };
  /*
    category	str	Filter results by category.
Accepted values: backgrounds, fashion, nature, science, education, feelings, health, people, 
religion, places, animals, industry, computer, food, sports, transportation, travel, buildings, business, music
    */
  return (
    <form onSubmit={SrchImg}>
      <div className="row">
        <div className="form-group col-md-5">
          <input
            type="text"
            className={ValidInput}
            value={SrchTxt}
            name="SrchTxt"
            placeholder="Escribi el nombre de la imgen que buscas"
            onChange={onChangeInput}
          />
        </div>
        <div className="form-group col-md-3">
          <select
            className="form-control col-md-12"
            value={SrchCat}
            name="SrchCat"
            onChange={onChangeInput}
          >
            <option value="">-Seleccione categoria-</option>
            <option name="All" value="All">
              Todas
            </option>
            <option name="backgrounds" value="backgrounds">
              Fondos
            </option>
            <option name="fashion" value="fashion">
              Moda
            </option>
            <option name="nature" value="nature">
              Naturaleza
            </option>
            <option name="science" value="science">
              Ciencia
            </option>
            <option name="education" value="education">
              Educacion
            </option>
            <option name="feelings" value="feelings">
              Sentimental
            </option>
            <option name="health" value="health">
              Salud
            </option>
            <option name="people" value="people">
              Personas
            </option>
            <option name="religion" value="religion">
              Religion
            </option>
            <option name="places" value="places">
              Lugares
            </option>
            <option name="animals" value="animals">
              Animales
            </option>
            <option name="industry" value="industry">
              Industria
            </option>
            <option name="computer" value="computer">
              Computacion
            </option>
            <option name="food" value="food">
              Alimentos
            </option>
            <option name="sports" value="sports">
              Deportes
            </option>
            <option name="transportation" value="transportation">
              Transporte
            </option>
            <option name="travel" value="travel">
              Viajes
            </option>
            <option name="buildings" value="buildings">
              Construcciones
            </option>
            <option name="business" value="business">
              Negocios
            </option>
            <option name="music" value="music">
              Musica
            </option>
          </select>
        </div>
        <div className="from-group col-md-4">
          <input
            type="submit"
            className="btn btn-lg btn-danger btn-block"
            value="Buscar"
          />
        </div>
      </div>
      {ErrorMsj ? <Error message="Ingresar datos para buscar" /> : null}
    </form>
  );
};

export default Form;
