import React from "react";
import face from '../Img/face.png'
import visto from '../Img/visto.png'

const Images = ({ image }) => {
  const { previewURL, largeImageURL, views, likes, tags } = image;

  return (
    <div className="col-12 col-sm-6 col-md-4 col-lg-3 mb-4">
      <div className="card">
        <img src={previewURL} alt={tags} className="card-img-top" />
      </div>
      <div className="card-body">

              <p className="card-text">
                 <span> {likes} &nbsp;&nbsp;&nbsp;
                  <img src={face} alt="" width="70px" height="70px"/>
                 </span>
              </p>

        <p className="card-text">
            <span> {views} &nbsp;&nbsp;&nbsp;
               <img src={visto} alt="" width="65px" height="50px"/>
            </span>
        </p>
      </div>
      <div className="card-footer">
        <a
          href={largeImageURL}
          target="_blank"
          rel="noopener"
          className="btn btn-primary btn-block"
        >
          Ver Imagen{" "}
        </a>
      </div>
    </div>
  );
};

export default Images;
